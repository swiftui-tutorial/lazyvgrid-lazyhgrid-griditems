//
//  lazyvgrid_lazyhgrid_griditemsApp.swift
//  lazyvgrid_lazyhgrid_griditems
//
//  Created by Kitti Jarearnsuk on 11/9/2565 BE.
//

import SwiftUI

@main
struct lazyvgrid_lazyhgrid_griditemsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
